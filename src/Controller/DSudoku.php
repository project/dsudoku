<?php

namespace Drupal\dsudoku\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the DSudoku module.
 */
class DSudoku extends ControllerBase {

  /**
   * Returns a 9x9 sudoku table.
   *
   * @return array
   *   A renderable array.
   */
  public function Page() {
    return [
      '#theme' => 'sudoku',
      '#sponsor'        => views_embed_view($name = "sudoku_sponsor", $display_id = 'default'),
      '#attached'       => [
        'library'       => 'dsudoku/default',
      ],
    ];
  }

}
